const form = document.querySelector('form');
form.addEventListener('submit', (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    fetch('/', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        if (data.status === 'success') {
            window.location.href = `/table/${data.file_name}`;
        } else {
            const responseDiv = document.querySelector('#response');
            responseDiv.innerHTML = data.status;
        }
    })
    .catch(error => console.error(error));
});