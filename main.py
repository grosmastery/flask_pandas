import json
import os

from flask import Flask, request, render_template, url_for, redirect, jsonify
import pandas as pd

app = Flask(__name__, static_url_path='/static')


@app.route('/', methods=['GET', 'POST'])
def main_page():
    if not os.path.exists('media'):
        os.makedirs('media')
    if request.method == 'GET':
        return render_template('main.html')
    elif request.method == 'POST':
        file = request.files['file']
        if file.filename.endswith('.csv') or file.filename.endswith('.json'):
            file_id = len([f for f in os.listdir('media/') if os.path.isfile(os.path.join('media/', f))])
            file.filename = f'{file_id}-{file.filename}'
            file.save(os.path.join('media', file.filename))
            return json.dumps({'status': 'success', 'file_name': file.filename})
        elif file.filename == '':
            return json.dumps({'status': 'No file selected'})
        else:
            return json.dumps({'status': 'File is not csv/json'})


@app.route('/table/<file_name>')
def file_table(file_name):
    if file_name.endswith('.csv'):
        df = pd.read_csv(os.path.join('media', file_name))
    else:
        df = pd.read_json(os.path.join('media', file_name))
    return render_template('table.html', data=df.fillna('').to_html(index=False))


if __name__ == '__main__':
    app.run(debug=True)



