# **Flask Project for Uploading and Displaying CSV/JSON Files**
This Flask project allows users to upload CSV or JSON files which are then displayed in an HTML table. The project includes a basic interface for file upload, error checking, and a simple table display. The project is intended as a portfolio.
## **Technologies Used**
- Python
- Flask
- Pandas
- HTML/CSS/JS
## **Usage**
1. Navigate to `localhost:5000` in a web browser
2. Click the `"Choose File"` button and select a CSV or JSON file to upload
3. Click the `"Upload"` button to upload the file
4. The uploaded file will be displayed in a table on a new page
